In JavaScript, there are several ways to access a child object other than using its identifier/name:

1. Dot notation: You can access a child object by using the dot (.) operator followed by the child object's name. For example:
   ```javascript
   var parent = {
     child: {
       name: "John"
     }
   };

   console.log(parent.child.name); // Output: John
   ```

2. Bracket notation: You can also use bracket notation to access a child object by providing the child object's name as a string within square brackets ([]). For example:
   ```javascript
   var parent = {
     child: {
       name: "John"
     }
   };

   console.log(parent["child"]["name"]); // Output: John
   ```

3. Object destructuring: If you have multiple nested objects and want to access specific properties directly, you can use object destructuring. For example:
    ```javascript
    var parent = {
      child1: { name: "John" },
      child2: { age: 25 }
    };

    const { child1 } = parent;
    console.log(child1.name); // Output: John

    const { age } = parent.child2;
    console.log(age); // Output: 25
    ```

4. Iteration methods (e.g., for...in loop): If you want to iterate over all properties of an object, including its children, you can use iteration methods like `for...in` loop or `Object.keys()`. For example:
    ```javascript
    var parent = {
      child1 : { name : "John" },
      child2 : { age : 25 }
    };

    for (var key in parent) {
      if (parent.hasOwnProperty(key)) {
        console.log(parent[key]);
      }
    }

    // Output:
    // { name: "John" }
    // { age: 25 }
    ```

These are some common ways to access child objects in JavaScript, but there may be other techniques depending on the specific use case.
# Tue 11 Jun 13:56:53 CEST 2024 - what other ways are there to access a child object in javascript than by its identifier/name?